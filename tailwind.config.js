module.exports = {
  theme: {
    extend: {
      colors: {},
      textColor: {
        black: "var(--color-text-black)",
        white: "var(--color-text-white)"
      },
      backgroundColor: {
        black: "var(--color-bg-black)",
        white: "var(--color-bg-white)"
      }
    }
  },
  variants: {},
  plugins: []
};
